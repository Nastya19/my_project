#include <iostream>
#include "conio.h"
#include "locale.h"
#include "math.h"
using namespace std;

int main()
{
    cout << "Hello";
    char c;
    int **mas = NULL;
    int *massum = NULL;
    int n, m, sum, l;

    setlocale(LC_ALL, "Russian");
    do
    {
        printf("1-Ввод и заполнение массива\n 2-Обработка массива\n 3-Вывод\n 0-Выход\n");
        c = _getch();
        switch (c)
        {
        case '1':
        {
            if (mas != NULL)
            {
                for (int i = 0; i < n; i++)
                {
                    delete[] mas[i];
                }
                delete[] mas;
                delete[] massum;
            }
            printf("Введите количество строк: ");
            scanf("%d", &n);
            printf("Введите количество столбцов: ");
            scanf("%d", &m);

            mas = new int*[n];
            for (int i = 0; i < n; i++)
            {
                mas[i] = new int[m];
            }
            massum = new int[n];


            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    printf("Введите mas[%d][%d]: ", i, j);
                    scanf("%d", &mas[i][j]);
                }
            }
            break;
        }
        case'2':
        {
            if (mas == NULL)
                break;
            for (int i = 0; i < n; i++)
            {
                sum = 0;
                for (int j = 0; j < m; j++)
                {
                    sum += abs(mas[i][j]);
                }
                massum[i] = sum;
            }
            bool sortok;
            do
            {
                sortok = true;
                for (int i = 0; i < n - 1; i++)
                {
                    if (massum[i] > massum[i + 1])
                    {
                        int t;
                        t = massum[i];
                        massum[i] = massum[i + 1];
                        massum[i + 1] = t;
                        int *tt = mas[i];
                        mas[i] = mas[i + 1];
                        mas[i + 1] = tt;
                        sortok = false;
                    }
                }
            } while (sortok == false);


            bool l = false;
            for (int j = 0; j < m; j++)
            {
                for (int i = 0; i < n; i++)
                {
                    if (mas[i][j] < 0)
                    {
                        printf("Номер столбца, содержащего первый отрицательный элемент   : %d\n", j);
                        l = true;
                        break;
                    }
                }
                if (l)
                {
                    break;
                }
            }
            if (l == false)
                printf("Нет столбцов с отрицательным элементом\n");
            break;
        }
        case'3':
        {
            if (mas == NULL)
                break;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    printf("%5d", mas[i][j]);
                }
                printf("\n");
            }

            break;
        }
        case'0':
        {
            break;
        }
        default:
            printf("Ошибка\n");
        }
    } while (c != '0');
    if (mas != NULL)
    {
        for (int i = 0; i < n; i++)
        {
            delete[] mas[i];
        }
        delete[] mas;
        delete[] massum;
    }
    return 0;
}
